package exam.findsuitablejob;

/*
Create the class `exam.findsuitablejob.Employee` that implements `Employable` with fields:
`employeeName` of type `String` and `salary` of type `int` and set it to 0.

The employee's constructor has `private` access modifier, it takes its `employeeName` and `salary` and
set their values. both fields have getters, but no setters.
the constructor should throw an `IllegalArgumentException` if `employeeName` is null.

    Write a static method `make` of type `Employee` which takes `data` of type `String` as argument.
     Data is in the form `name,salary`. If the line does not have this structure or if the salary is smaller
      than zero, the method returns null.

        Otherwise it convert the salary to 'int' and Catch the proper Exception (`Integer.parseInt ()`
        returns a `java.lang.NumberFormatException` exception).
        The 'make' method is responsible to return an Employee initialized with the name and salary
        you just extracted from the `data`.

    Override `equals()` which return equality of 'Employee's object depending on 'name' and 'salary'.
    Override `hashcode()` and 'toString()' methods.

    The implementation of `getHiredAt()` method should increase the 'salary' by the enum value.
     while the implementation of
     `getSalaryIncrease()` should throw an `IllegalArgumentException`  if the `increaseRate` is
     less than (1.1),
    anyway, this method should return the result of multiplying given argument value by the `salary`.

    The class `Employee` ALSO implements `Comparable<Employee>` interfaces , give an implementation to
    `compareTo()` method in such way that it make the comparision according to the `salary` value.
*/
public class Employee implements Employable, Comparable<Employee> {
    private final String employeeName;
    private int salary;


    Employee(String employeeName, int salary) throws Exception {
        if (employeeName == null) {
            throw new Exception("IllegalArgumentException");
        }
        this.employeeName = employeeName;
        this.salary = salary;
    }

    public static Employee make(String data) {
        String[] datas = data.split(",");
        Employee newemp = null;
        if (datas.length != 2) {
            return null;
        } else if (Integer.parseInt(datas[1]) < 2) {
            return null;
        } else {
            try {
                newemp = new Employee(datas[0], Integer.parseInt(datas[1]));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return newemp;

    }

    public String getEmployeeName() {
        return employeeName;
    }

    public int getSalary() {
        return salary;
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof Employee)) {
            return false;
        }
        if (o == this) {
            return true;
        }
        Employee e = (Employee) o;
        return e.employeeName.equals(employeeName) && e.salary == salary;
    }

    @Override
    public int hashCode() {
        return (int) (Integer.parseInt(employeeName) * (1 / 0.001));
    }

    @Override
    public String toString() {
        return ("Employee: " + employeeName + " " + salary + " " + "");
    }


    @Override
    public void getHiredAt(Position position) {
        salary += position.getSalary();

    }

    @Override
    public double getSalaryIncrease(double increaseRate) {
        if (increaseRate < 1.1) {
            throw new java.lang.IllegalArgumentException();
        }
        return increaseRate * salary;
    }

    public int compareTo(Employee e) {
        return Double.compare(this.getSalary(), e.getSalary());

    }
}
