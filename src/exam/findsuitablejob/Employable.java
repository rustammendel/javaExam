package exam.findsuitablejob;
/*
 Create the interface `exam.findsuitablejob.Employable` with the method `getHiredAt()`
  that takes a `Position` and the method
   `getSalaryIncrease()` that takes `increaseRate` of type `double` as a parameter.*/

public interface Employable {
    double getSalaryIncrease(double increaseRate) throws Exception;

    void getHiredAt(Position position);
}
