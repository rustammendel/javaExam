package exam.findsuitablejob;

/*
Create an `exam.findsuitablejob.Contract` class representing a contract, has the following fields:
        `degree` of type String , `numberOfEmployees` of type int , `contractPeriod` of type int.
        It also has a static field: `goalSalary`. this should set to 4000 initially.
     Make all fields of `Contract` class accessible to the descendants of `Contract`, but not to any other class.
     Create getters for all of them, and a setter only for `goalSalary`.
    `Contract` has two constructors:
        A constructor that takes the `degree`, `numberOfEmployees` and `contractPeriod` as parameters and sets
         the corresponding fields. In it, make sure that the following conditions hold:
             `contractPeriod` is at least 6 and not more than 12 months,
             (for this create two constants( outside the constructor) that hold values 6 and 12 of type 'int'.
            `numberOfEmployees` is at least 15 and not more than 40 .
             If either one is incorrect, throw an `IllegalArgumentException`.
             To make it easier to check those conditions,
             create a static method `isBetween(number, min, max)`
             that returns whether `number` is between `min` and `max`.
        A constructor that takes no parameters. It calls the previously defined constructor with the following values:
             `degree`: "Master"
             `numberOfEmployees` : 20
             `contractPeriod` : 12
     Override `toString()` method and give it a suitable implementation.
*/

public class Contract {
    protected static int goalSalary = 4000;
    private static final int MINCON = 6;
    private static final int MAXCON = 12;
    protected String degree;
    protected int numberOfEmployees;
    protected int contractPeriod;


    public Contract() {
        this("Master", 20, 12);
    }

    public Contract(String degree, int numberOfEmployees, int contractPeriod) {
        if (!isBetween(contractPeriod, MINCON, MAXCON) || !isBetween(numberOfEmployees, 15, 40)) {
            throw new java.lang.IllegalArgumentException();
        }
        this.degree = degree;
        this.numberOfEmployees = numberOfEmployees;
        this.contractPeriod = contractPeriod;
    }

    public static int getGoalSalary() {
        return goalSalary;
    }

    static boolean isBetween(int number, int min, int max) {
        return (number <= max && number >= min);
    }

    public String getDegree() {
        return degree;
    }

    public int getNumberOfEmployees() {
        return numberOfEmployees;
    }

    public int getContractPeriod() {
        return contractPeriod;
    }

    @Override
    public String toString() {
        return ("Contract: " + degree + " " + numberOfEmployees + " " + contractPeriod);
    }
}
