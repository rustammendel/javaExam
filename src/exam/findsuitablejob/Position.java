package exam.findsuitablejob;

/*

Create an enum `exam.findsuitablejob.Position` which includes the four positions:
FREELANCER, UNIVERSITY,HOSPITAL and ITCOMPANY.

    Each of these enum values have one `int` that indicate the  associated `salary`, as following:
     `FREELANCER` has 7000, 'UNIVERSITY' has 4000 , 'HOSPITAL' has 5000 and 'ITCOMPANY' has 2700.
    Create getter for `salary`.
    Set a constructor for `Position` enum that takes `salary` as argument and set its value.

*/
public enum Position {
    FREELANCER(7000), UNIVERSITY(4000), HOSPITAL(5000), ITCOMPANY(2700);
    private final int salary;

    Position(int salary) {
        this.salary = salary;
    }

    public float getSalary() {
        return this.salary;
    }
}

