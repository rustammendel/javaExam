package exam.findsuitablejob;
/*
Create `exam.findsuitablejob.EmpAgency`, a child class of `Contract`.
    It has a field called `employees`, a list of `Employee' objects.
    Its only constructor takes `degree`, `contractPeriod` and an arbitrary number of `Employee`s and
    stores them in its fields.
    Create method `load()` which takes `filePath` of type `String` and return an Array of 'Employee' objects.
     It will read from an input file line by line, each line will be passed to `Employee.make()` to create a
     new employee. If a line is 'null' you should skip it. Make sure to catch 'IOException' since you are
     reading a file.

    Create method `hire()` that takes a 'Position' as argument and it go through 'employees' list
     (using Iterator). Then the list of hired employees is sorted in ascending order depending on 'salary'value.
    If an employee's salary after being hired, is equal or less than the 'goalSalary', then hire him/her with
    'getHiredAt()' method and remove the employee from `employees` and update `numberOfEmployees`.
     Remember to use `Iterator` to achieve that.

    Create method `getMaxIncrease()` which takes an argument of type 'double' and throw
    `IllegalStateException` if the number of employees is 0; otherwise it will calculate the increase of
    the salary of each employee in `employees` list depending on the given argument, and it will
     return maximum increase value among them ('maxIncrease' of type double).

    Override `toString()` in `EmpAgency` which prints employees.
    */

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmpAgency extends Contract {

    ArrayList<Employee> employees = new ArrayList<>();

    public EmpAgency(String degree, int contractPeriod, Employee... _employees) throws Exception {
        super(degree, _employees.length, contractPeriod);
        for (Employee emp : _employees) {
            employees.add(emp);

        }
    }

    public static List<Employee> load(String filePath) {
        List<Employee> result = new ArrayList<>();

        try {
            File file = new File(filePath);
            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
//
//                String delims = "[,]";
//                String[] strarray = line.split(delims);
//
//                String ename = strarray[0];
//                int sal = Integer.parseInt(strarray[1]);

                result.add(Employee.make(line));


            }
        } catch (Exception e) {
            System.out.println("File not found");

        }


        return result;

    }

    /* Create method `hire()` that takes a 'Position' as argument and it go through 'employees' list
         (using Iterator).
         Then the list of hired employees is sorted in ascending order depending on 'salary' value.
        If an employee's salary after being hired, is equal or less than the 'goalSalary', then hire him/her with
        'getHiredAt()' method and remove the employee from `employees` and update `numberOfEmployees`.
         Remember to use `Iterator` to achieve that.*/
    public void hire(Position position) {
        for (Employee empl : employees) {
            System.out.println(empl);
        }
        employees.sort(Employee::compareTo);

        for (Employee empl : employees) {
//            System.out.println(empl);
            if (empl.getSalary() <= getGoalSalary()) {
                empl.getHiredAt(position);
                employees.remove(empl);
                super.numberOfEmployees--;

            }
        }

    }

    /*
        Create method `getMaxIncrease()` which takes an argument of type 'double' and throw
        `IllegalStateException` if the number of employees is 0; otherwise it will calculate the increase of
        the salary of each employee in `employees` list depending on the given argument, and it will
         return maximum increase value among them ('maxIncrease' of type double).
         */
    public double getMaxIncrease(double incr) throws Exception {
        double max = 0;
        if (employees.size() <= 0) {
            throw new Exception("IllegalStateException");
        }
        for (Employee empl : employees) {
            if (empl.getSalary() * incr > max) {
                max = empl.getSalary() * incr;
            }
        }

        return max;
    }

    @Override
    public String toString() {
        return ("Empagency : " + degree + " " + contractPeriod + " " + employees);
    }

}
